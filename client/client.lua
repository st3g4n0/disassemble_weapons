ESX    				= nil
local PlayerData    = {}

Citizen.CreateThread(function()
	while ESX == nil do
	  TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	  Citizen.Wait(100)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(playerData)
	PlayerData = playerData;
end)

RegisterCommand(Config.CommandName, function(source, args, rawCommand)
	menu()
end, false)
TriggerEvent('chat:addSuggestion', '/'..Config.CommandName, Config.Locale['COMMAND-SUGGESTION'])

function menu()
	ESX.TriggerServerCallback('MTCore:getLoadout', function(loadout)
		local elements = {}
		for k,v in ipairs(loadout) do
			table.insert(elements, {label = v.label, name = v.name, ammo = v.ammo})
		end

		ESX.UI.Menu.CloseAll()
		ESX.UI.Menu.Open("default",	GetCurrentResourceName(), "menu_disassemble",
		{
			title = Config.Locale['TITLE'],
			align = Config.MenuAlign,
			elements = elements
		},
		function(data, menu)
			TriggerServerEvent("MTCore:disassemble", data.current.name, data.current.ammo)
			menu.close()
		end,
		function(data, menu)
			menu.close()
		end)
	end)
end

