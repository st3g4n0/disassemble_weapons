# disassemble_weapons

## Download & Installation

### Using Git
```
cd resources
git clone https://gitlab.com/st3g4n0/disassemble_weapons.git [esx]/disassemble_weapons
```

### Manually
- Download https://gitlab.com/st3g4n0/disassemble_weapons/
- Put it in the `[esx]` directory

## Installation
- Import `disassemble.sql` to your database
- Add this in your `server.cfg`:

```
start disassemble_weapons
```

# Legal
### License
disassemble_weapons - Disassemble weapon and bullets

Copyright (C) 2021 Vladimir Mateev

This program Is free software: you can redistribute it And/Or modify it under the terms Of the GNU General Public License As published by the Free Software Foundation, either version 3 Of the License, Or (at your option) any later version.

This program Is distributed In the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty Of MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License For more details.

You should have received a copy Of the GNU General Public License along with this program. If Not, see http://www.gnu.org/licenses/.