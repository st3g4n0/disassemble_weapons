Config = {}
Config.Webhook = 'Discord Webhook URL'
Config.Name = 'Discord Bot Name'
Config.CommandName = 'desmontar'
Config.MenuAlign = 'bottom-right'

Config.Locale = {
    ['TITLE'] = 'Desmontar arma',
    ['COMMAND-SUGGESTION'] = 'Desmonta el arma y sus balas y guardalos en el inventario.',
    ['INVALID-WEIGHT'] = 'El jugador no puede llevar tanto peso encima.',
    ['ERROR-DISSASEMBLY'] = 'No puedes desmontar esta arma.',
    ['FIRST-DISSASEMBLY'] = 'Tienes que montar primero el arma.',
    ['ALREADY-HAVE'] = 'Ya tienes la misma arma montada.',
}

Config.AllowedWeapons = {
    ['WEAPON_PISTOL'] = true,
    ['WEAPON_ASSAULTRIFLE'] = true,
    ['WEAPON_PUMPSHOTGUN'] = true,
    ['WEAPON_SMOKEGRENADE'] = true,
    ['WEAPON_MICROSMG'] = true,
    ['WEAPON_FLASHLIGHT'] = true,
    ['WEAPON_PETROLCAN'] = true,
    ['WEAPON_SMG'] = true,
    ['WEAPON_CARBINERIFLE'] = true,
    ['WEAPON_MARKSMANRIFLE'] = true,
    ['WEAPON_NIGHTSTICK'] = true,
    ['WEAPON_COMBATPISTOL'] = true,
    ['WEAPON_BZGAS'] = true,
    ['WEAPON_APPISTOL'] = true,
    ['WEAPON_PISTOL50'] = true,
    ['WEAPON_ASSAULTSMG'] = true,
    ['WEAPON_VINTAGEPISTOL'] = true,
    ['WEAPON_MUSKET'] = true,
    ['WEAPON_SPECIALCARBINE'] = true,
    ['WEAPON_SWITCHBLADE'] = true,
    ['WEAPON_KNUCKLE'] = true,
    ['WEAPON_SNSPISTOL'] = true,
    ['WEAPON_KNIFE'] = true,
    ['WEAPON_HEAVYPISTOL'] = true,
    ['WEAPON_SWITCHBLADE'] = true,
}

Config.WhiteWeapons = {
    ['WEAPON_KNIFE'] = true,
    ['WEAPON_SWITCHBLADE'] = true,
}
    