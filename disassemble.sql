﻿INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
('WEAPON_PISTOL', 'Pistola', 20, 0, 1),
('WEAPON_ASSAULTRIFLE', 'Fusil de asalto', 2, 0, 1),
('WEAPON_PUMPSHOTGUN', 'Escopeta', 2, 0, 1),
('WEAPON_SMOKEGRENADE', 'Granada de gas', 25, 0, 1),
('WEAPON_MICROSMG', 'Micro SMG', 5, 0, 1),
('WEAPON_FLASHLIGHT', 'Linterna', 20, 0, 1),
('WEAPON_PETROLCAN', 'Bidón de gasolina', 2, 0, 1),
('WEAPON_SMG', 'SMG', 2, 0, 1),
('WEAPON_CARBINERIFLE', 'Fusil carabina de asalto', 2, 0, 1),
('WEAPON_MARKSMANRIFLE', 'Rifle de francotirador', 2, 0, 1),
('WEAPON_NIGHTSTICK', 'Porra', 10, 0, 1),
('WEAPON_COMBATPISTOL', 'Pistola de combate', 10, 0, 1),
('WEAPON_STUNGUN', 'Taser', 10, 0, 1),
('WEAPON_BZGAS', 'Gas BZ', 25, 0, 1),
('WEAPON_APPISTOL', 'Pistola AP', 10, 0, 1),
('WEAPON_PISTOL50', 'Pistola Calibre 50', 5, 0, 1),
('WEAPON_ASSAULTSMG', 'SMG de asalto', 2, 0, 1),
('WEAPON_VINTAGEPISTOL', 'Pistola Vintage', 15, 0, 1),
('WEAPON_MUSKET', 'Mosquete', 2, 0, 1),
('WEAPON_SPECIALCARBINE', 'Carabina especial', 2, 0, 1),
('WEAPON_SWITCHBLADE', 'Navaja', 20, 0, 1),
('WEAPON_KNUCKLE', 'Puño americano', 20, 0, 1),
('WEAPON_SNSPISTOL', 'Pistola SNS', 15, 0, 1),
('WEAPON_HEAVYPISTOL', 'Pistola pesada', 20, 0, 1),
('mWEAPON_PISTOL', 'Munición de pistola', 1000, 0, 1),
('mWEAPON_ASSAULTRIFLE', 'Munición de fusil de asalto', 1000, 0, 1),
('mWEAPON_PUMPSHOTGUN', 'Munición de escopeta', 1000, 0, 1),
('mWEAPON_MICROSMG', 'Munición de microsmg', 1000, 0, 1),
('mWEAPON_SMG', 'Munición de smg', 1000, 0, 1),
('mWEAPON_CARBINERIFLE', 'Munición de carabina de asalto', 1000, 0, 1),
('mWEAPON_MARKSMANRIFLE', 'Munición de rifle de francotirador', 1000, 0, 1),
('mWEAPON_COMBATPISTOL', 'Munición de pistola de combate', 1000, 0, 1),
('mWEAPON_APPISTOL', 'Munición de pistola ap', 1000, 0, 1),
('mWEAPON_PISTOL50', 'Munición de pistola calibre 50', 1000, 0, 1),
('mWEAPON_ASSAULTSMG', 'Munición de smg de asalto', 1000, 0, 1),
('mWEAPON_VINTAGEPISTOL', 'Munición de pistola vintage', 1000, 0, 1),
('mWEAPON_MUSKET', 'Munición de mosquete', 1000, 0, 1),
('mWEAPON_SPECIALCARBINE', 'Munición de carabina especial', 1000, 0, 1),
('mWEAPON_SNSPISTOL', 'Munición de pistola sns', 1000, 0, 1),
('mWEAPON_HEAVYPISTOL', 'Munición de pistola pesada', 1000, 0, 1);
